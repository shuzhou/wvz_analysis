# WVZ_Analysis
=======================================

1.Basic info:

---------------------------------------
Codes to produce ntuples are under `xAOD/Code_Rel21`. This framework is base on QuickAna, maintained by Zirui Wang:`https://gitlab.cern.ch/ziwang/quickana`.

DNN framework is under `DNN`, which is used in bbll analysis. 

There are several GPU machine you could used to MVA study: `umt3gpu01.aglt.org`,`gpu0(1-5)atum.cern.ch`(need to login via lxplus).



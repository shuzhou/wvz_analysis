#!/bin/bash

# Receive Arguments from command lines
path=$1
workpath=$2 
user=$3
STREAM=$4
jobNode=`hostname`

# Create an uniquename for the run folder
unique_tag1=`basename $path | cut -d'.' -f1-2`
unique_tag2=`date +%s`
localdir="/tmp/${user}/mytest/${workpath}-${unique_tag1}-${unique_tag2}-${jobNode}"
#localdir="/atlas/data18a/bili/work/submit_tmp/mytest/${workpath}-${unique_tag1}-${unique_tag2}"
workdir=$path"/"$workpath

echo "..................................................................................."
echo "  Runing Jobs using host $jobNode in directory $workdir..."
echo "..................................................................................."

#### create local working directory and copy input files to local disk

mkdir -vp $localdir

#cp -rf $path/Code/*                           $localdir/

#### setup
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
setupATLAS
asetup AnalysisBase,21.2.64
cd /atlas/data18a/shuzhouz/di-higgs/21.2.64/Code_Rel21
source setup.sh
make
#### change to local directory and run jobs
cd $localdir
#mkdir $workdir/output
cp $workdir/input_list.txt input_list.txt
#### run job
runFourLep -outdir $workdir/output -in input_list.txt


#### delete information in local disk
cd /tmp/$user/mytest
rm -r -f $localdir

########################################################

ExitStatus=$?
if [ $ExitStatus -ne 0 ]; then
  echo "e-id job failed with status $ExitStatus ..."
  exit $ExitStatus
fi

exit 0


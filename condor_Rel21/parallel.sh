#!/bin/bash
current=`date "+%Y-%m-%d %H:%M:%S"`
timeStamp=`date -d "$current" +%s`
#将current转换为时间戳，精确到毫秒
currentTimeStamp=$((timeStamp*10+`date "+%N"`/100000000))
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
setupATLAS
asetup AnalysisBase,21.2.64
output="/lustre/umt3/user/shuzhouz/dihiggs/condor_output_0202/condor_output_16d-${currentTimeStamp}"
work="/atlas/data18a/shuzhouz/di-higgs/condor_Rel21"
sourced="/atlas/data18a/shuzhouz/di-higgs/21.2.64/Code_Rel21"
rm -rf $output
mkdir $output
rm -rf $work/list
mkdir $work/list
cd $work/list
cp ../input_list.txt .
../split_inputfile.sh
rm -f input_list.txt
files=$(ls $work/list)
for filename in $files
do
  mkdir $output/$filename
#mkdir $output/$filename/output
done

cd $sourced
source setup.sh
cd $sourced/build
make -j30
#cd $work
cd $sourced/build
Njob=10
Nproc=30


function CMD {
n=$((RANDOM % 5 + 1))
echo "Job $1 Ijob $2 sleeping for $n seconds ..."
sleep $n
echo "Job $1 Ijob $2 exiting ..."
}

Pfifo="/tmp/$$.fifo"
mkfifo $Pfifo
exec 6<>$Pfifo

rm -f $Pfifo

# 在fd6中放置$Nproc个空行作为令牌
for((i=1; i<=$Nproc; i++)); do
echo
done >&6
j=0
for filename1 in $files
do  # 依次提交作业
read -u6                   # 领取令牌, 即从fd6中读取行, 每次一行
# 对管道，读一行便少一行，每次只能读取一行
# 所有行读取完毕, 执行挂起, 直到管道再次有可读行
# 因此实现了进程数量控制
{      # 要批量执行的命令放在大括号内, 后台运行
j=j+1
runFourLep -outdir $output/$filename1/output -in $work/list/$filename1 > $output/$filename1/log.txt
CMD $i && {            # 可使用判断子进程成功与否的语句
echo "Job $j finished"
} || {
echo "Job $j error"
}
sleep 1     # 暂停1秒，可根据需要适当延长,
# 关键点，给系统缓冲时间，达到限制并行进程数量的作用
echo >&6    # 归还令牌, 即进程结束后，再写入一行，使挂起的循环继续执行
} &

done

wait                # 等待所有的后台子进程结束
exec 6>&-           # 删除文件标识符

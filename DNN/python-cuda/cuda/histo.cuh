#ifndef HISTO       
#define HISTO       

#include <stdio.h>

extern "C" 
void histo(float *input_vec, int *output_vec, float min, float max, long long size, int bins);
#endif

#ifndef SCAN       
#define SCAN       

#include <stdio.h>

extern "C" 

void scan(float* input_vec, float* output_vec, long long size, int isExclusive);
    
#endif

#ifndef REDUCE_MIN       
#define REDUCE_MIN       

#include <stdio.h>

extern "C" 

float reduce_min(float* input_vec, long long size);
    
#endif

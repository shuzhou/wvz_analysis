#ifndef SEARCH       
#define SEARCH       

#include <stdio.h>

extern "C" 

int search(float *input_vec1, float *input_vec2, int size1, int size2);
    
#endif

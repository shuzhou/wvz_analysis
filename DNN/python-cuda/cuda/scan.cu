#include <stdio.h>
#include <math.h>
#include <cstdlib>
#include "scan.cuh"

__global__ void gpu_exclusive(float *input_vec, float *output_vec, long long size){
	int index=blockIdx.x*blockDim.x+threadIdx.x;
	if(index<size){
		output_vec[index]=output_vec[index]-input_vec[index];
	}
}


__global__ void gpu_simple_scan(float *input_vec, float *output_vec, long long size){
 		extern __shared__ float temp_in[];
		extern __shared__ float temp_out[];
		int tid=threadIdx.x;
		int index=blockIdx.x*blockDim.x+threadIdx.x;
		if(index<size){
			temp_in[tid]=input_vec[index];
		}
		else{
			temp_in[tid]=0.0;
		}
		__syncthreads();
		for(int i=1;i<blockDim.x;i=i*2){
			temp_out[tid]=temp_in[tid];
			__syncthreads();
			if(tid+i<blockDim.x){
				temp_out[i+tid]=temp_in[tid]+temp_in[i+tid];
			}
			__syncthreads();
			temp_in[tid]=temp_out[tid];
			__syncthreads();
		}
		if(index<size){
			output_vec[index]=temp_out[tid];
		}
}

__global__ void gpu_group_scan_dis(float *input_vec, float *output_vec, int step_size, int num_bl, long long size){
	int bl_size = step_size/num_bl;
	int num_step = blockIdx.x/num_bl;
	int bid = blockIdx.x;
	int tid = threadIdx.x;
	int section_num = bid%num_bl;
	int location = (2*num_step+1)*step_size-1;
	float value;
	if(location<size){
		value=input_vec[location];
	}
	else{
		value=0;
	}
	int loc_temp;
	for(int i=tid; i<bl_size; i=i+blockDim.x){
		loc_temp = (2*num_step+1)*step_size+bl_size*section_num+i;
		if(loc_temp<size){
			output_vec[loc_temp]=input_vec[loc_temp]+value;
		}
		loc_temp = (2*num_step)*step_size+bl_size*section_num+i;
		if(loc_temp<size){
			output_vec[loc_temp]=input_vec[loc_temp];
		}
	}
}


void scan(float *input_vec, float *output_vec, long long size, int isExclusive){
	float *a;
	float *out;
	const int num_threads=1024;
	int step_size=1024;
	int num_blocks;
	int deviceId;
	int numberOfSMs;
	int numblocks;
	int count;
	cudaError_t addVectorsErr;
	cudaError_t asyncErr;
	cudaGetDevice(&deviceId);
	cudaDeviceGetAttribute(&numberOfSMs, cudaDevAttrMultiProcessorCount, deviceId);
	num_blocks=size/num_threads+1;
	dim3 block_num(num_blocks,1,1);
	dim3 threads_per_block(num_threads,1,1);
	cudaMallocManaged(&a,size*sizeof(float));
	cudaMemcpy(a, input_vec, size*sizeof(float),cudaMemcpyHostToDevice);
	cudaMallocManaged(&out,size*sizeof(float));
	cudaMemPrefetchAsync(out, size*sizeof(float), deviceId);
	gpu_simple_scan<<<block_num,threads_per_block,num_threads*sizeof(float)>>> (a, out, size);
	cudaDeviceSynchronize();
	count = 1;
	for(double i =(double)size/((double)step_size*2.0);i>0.5;i=i/2.0){
	//for(int i=0;i<2;i++){
	        float *temp_out;
		cudaMallocManaged(&temp_out, size*sizeof(float));
		cudaMemPrefetchAsync(temp_out, size*sizeof(float),deviceId);
		numblocks = size/(step_size*2);
		if(numblocks<(double)size/((double)step_size*2.0)){
			numblocks=numblocks+1;
		}
		if(numblocks<128){
			count=count*2;
		}
		dim3 block_num1(numblocks*count,1,1);
		gpu_group_scan_dis<<<block_num1,threads_per_block>>>(out, temp_out,step_size,count,size);
		addVectorsErr = cudaGetLastError();
		if(addVectorsErr != cudaSuccess) printf("Error: %s\n", cudaGetErrorString(addVectorsErr));
		asyncErr = cudaDeviceSynchronize();
		if(asyncErr != cudaSuccess) printf("Error: %s\n", cudaGetErrorString(asyncErr));
		cudaDeviceSynchronize();
		cudaMemcpy(out, temp_out, size*sizeof(float),cudaMemcpyDeviceToDevice);
		cudaFree(temp_out);
		step_size=step_size*2;
	}
	if(isExclusive==1){
		gpu_exclusive<<<block_num,threads_per_block>>>(a,out,size);
		cudaDeviceSynchronize();
	}
	cudaMemcpy(output_vec, out, size*sizeof(float),cudaMemcpyDeviceToHost);
}

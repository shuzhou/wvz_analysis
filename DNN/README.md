MVA in bbll
========================

1.Base info:
-------------------------

This code is used to train DNN model and add DNN score to minitree in HH-bbll analysis.

Major MVA method used: DNN and XGBoost. DNN has better performance. XGBoost is used to do cross check and get features ranking.

DNN package: Pytorch. reference: https://pytorch.org/

Install package: conda install pytorch torchvision cudatoolkit=10.0 -c pytorch

pip install xgboost

This code is writed and tested using Python 3.7 and CentOS 7.6.8, cuda version 10.2

Main codes are under `python/bbll`:
  - `preprocess.py` automatic load minitrees using pyROOT and create dataFrames for training and testing.
  - `model.py` different models to create DNN. You can use different config net parameters to construct different network.
  - `network_core.py` Network construction code.


You just need to write config parameters to do the DNN training and applying to minitrees. Both regular DNN and parameterized DNN are support, using `pDNN = True` in configure parameters to use parameterized DNN. ResNet is also supported, using `model_type = 3` and other Res_parameters to construct customized ResNet. Use `model_type = 2` to construct customized linear network.


Sample code to do the DNN training and applying back to minitrees is `jupyter/DNN-updated-test.ipynb`
To use BDT(XGBoost), the sample code is `jupyter/XGBoost-test.ipynb`

2.GPU computing
------------------------------------

GPU parallel computing is used to speed up this analysis, espeically input feature normalization and count event yield pass cuts.

All cuda realated codes are merged into package `cuda_guass_normal`. URL: https://pypi.org/project/cuda-guass-normal/

pip install cuda_guass_normal

Source code: `cuda_guass_normal/__init__.py`. Read README to get more information about normalization.


If no GPU avaliable, just remove it and use simple function in python and set device("cpu") instead of device("cuda:0")







import torch
from torch.utils.data import DataLoader, TensorDataset
from torch import Tensor
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
# Optimizer SWATS
import swats
#numpy, pandas, matplotlib
import numpy as np
import cuda_guass_normal
from importlib import reload
import pandas as pd
import time

from array import array
from .model import *
from .train import *
from .preprocess import *


class core:
    def __init__(self, parameters):
        self.parameters = parameters
        self.dim = self.parameters["input_size"]
        self.model_type = self.parameters["model_type"]
        self.num_class = self.parameters["num_class"]
        if((self.model_type == 2)|(self.model_type == 4)):
            self.dim_list = self.parameters["dim_list"]
            self.drop_out = self.parameters["drop_out"]
            self.drop_out_rate = self.parameters["drop_out_rate"]
        if(self.parameters["device"] =="gpu"):
            self.device = torch.device("cuda:0")
        else:
            self.device = torch.device("cpu")
        self.num_cpu = self.parameters["num_cpu"]
        self.optim = self.parameters["optimizer"]
        if(self.model_type == 3):
            self.res_param = self.parameters["Res_parameters"]
        if(self.model_type == 4):
            self.rnn_param = self.parameters["rnn_parameters"]
        if(self.num_class == 2):
            if(self.parameters["device"] =="gpu"):
                self.loss_function = nn.BCELoss().cuda()
            else:
                self.loss_function = nn.BCELoss()
        else:
            if(self.parameters["device"] =="gpu"):
                self.loss_function = nn.CrossEntropyLoss(reduction='none').cuda()
            else:
                self.loss_function = nn.CrossEntropyLoss(reduction='none')
        self.model, self.optimizer = self._build_model()    
    def _build_model(self):
        if(self.model_type == 1): # test naive ResNet
            model = nn.Sequential(nn.utils.weight_norm(nn.Linear(self.dim,100),name='weight'),Mish(),
                                  ResNet(100),ResNet(100),ResNet(100),ResNet(100),
                                  nn.utils.weight_norm(nn.Linear(100, 100),name='weight'),Mish(),
                                  nn.utils.weight_norm(nn.Linear(100, 1),name='weight'),nn.Sigmoid()).to(self.device)
        elif(self.model_type == 0): # test naive linear net
            model = Net(self.dim).to(self.device)
        
        elif(self.model_type == 2): # custom linear net
            param = generate_parameter(self.dim_list,self.drop_out,self.drop_out_rate)
            param["num_class"] = self.num_class
            model = generate_linear_model(param, self.device)
            
        elif(self.model_type == 3): # custom Resnet
            self.res_param["num_class"] = self.num_class
            model = generate_ResNet(self.res_param, self.device)
        elif(self.model_type == 4): #custom linear net with one RNN at first layer to handle variable input size
            param = generate_parameter(self.dim_list,self.drop_out,self.drop_out_rate)
            param["num_class"] = self.num_class
            param["rnn_parameters"] = self.rnn_param
            model = generate_RNN_model(param, self.device)
            
            
        model.apply(init_weights)
        if(self.optim["type"] == "swats"):
            optimizer = swats.SWATS(model.parameters())
        elif(self.optim["type"] == "sgd"):
            optimizer = optim.SGD(model.parameters(), lr=self.optim["lr"], momentum=self.optim["beta"])
        elif(self.optim["type"] == "adam"):
            optimizer = optim.Adam(model.parameters(), lr=self.optim["lr"])
        elif(self.optim["type"] == "lbfgs"):
            optimizer = optim.LBFGS(model.parameters(),lr=self.optim["lr"])
        return(model, optimizer)
    
    
    
    def train(self, train_data, train_target, train_weight, eval_data, eval_target, eval_weight, train_batch, test_batch, max_epoch, patient):
        num_class = self.num_class
        train_data_tensor = torch.tensor(train_data, dtype=torch.float)
        train_target_tensor = torch.tensor(train_target, dtype=torch.float)
        train_weight_tensor = torch.tensor(train_weight, dtype=torch.float)
        #if(self.num_class>2):
            #train_target_tensor = one_hot_embedding(train_target_tensor, num_class) # transfer label for softmax output
        trainDataset = TensorDataset(train_weight_tensor, train_data_tensor, train_target_tensor)
         
        test_data_tensor = torch.tensor(eval_data, dtype=torch.float)
        test_target_tensor = torch.tensor(eval_target, dtype=torch.float)
        test_weight_tensor = torch.tensor(eval_weight, dtype=torch.float)
        #if(self.num_class>2):
            #test_target_tensor = one_hot_embedding(test_target_tensor, num_class)
        testDataset = TensorDataset(test_weight_tensor, test_data_tensor, test_target_tensor)
        if(self.parameters["device"] =="gpu"):
            trainLoader = DataLoader(trainDataset, batch_size=train_batch,shuffle=False,
                                      num_workers=self.num_cpu,pin_memory=True)
            testLoader = DataLoader(testDataset, batch_size=test_batch, shuffle=False,
                                    num_workers=self.num_cpu,pin_memory=True)
        else:
            trainLoader = DataLoader(trainDataset, batch_size=train_batch,shuffle=False)
            testLoader = DataLoader(testDataset, batch_size=test_batch, shuffle=False)
            
        if(self.optim["type"] == "lbfgs"):
            loss_np, loss_test_np, best_epoch=train_model_lbfgs(self.model, self.device, trainLoader, testLoader, self.loss_function, self.optimizer, max_epoch, patient, num_class)
        else:
            loss_np, loss_test_np, best_epoch=train_model(self.model, self.device, trainLoader, testLoader, self.loss_function, self.optimizer, max_epoch, patient, num_class)
            
    def predict(self, data, batch):
        dataTestTensor= torch.tensor(data, dtype=torch.float,device=self.device)
        dataTestLoader=DataLoader(dataTestTensor,batch_size=batch, shuffle=False)
        score = eval_pytorch(self.model,self.device,dataTestLoader, self.num_class)
        if(self.num_class == 2):
            return(score[0:,0])
        else:
            return(score[0:,:])
    
    def save_model(self, model_name):
        torch.save(self.model.state_dict(),model_name)
        print('model saved to:', model_name)
        
    def load_model(self, model_name):
        self.model.load_state_dict(torch.load(model_name))
        print('loaded model',model_name)
        
    def inputs_transfer(self, class_num, mean_vec, var_vec, num_epoch = 20, lr=1.0): # calculate inputs that can get highest score from the model
        device = self.device 
        dim = self.dim
        inputs = torch.randn((1,dim),device=device)
        loss_fn = self.loss_function
        num_class = self.num_class
        if(self.num_class>2):
            target = torch.tensor([class_num],dtype = torch.long).to(device)
        else:
            target = torch.tensor([class_num],dtype = torch.float32).to(device)
        
        output = inputs_transfer(self.model,inputs,target, loss_fn,num_class, num_epoch,lr)
        output_np = output.data.cpu().numpy()
        output_np = output_np.reshape(-1)
        output_np = output_np*np.sqrt(var_vec.reshape(-1)+0.001)+mean_vec.reshape(-1)
        self.model.eval()
        y_hat = self.model(output)
        if(num_class>2):
            y_hat = nn.functional.softmax(y_hat, dim = 1)
        y_np = y_hat.data.cpu().numpy()
        output_np=output_np.reshape((1,dim))
        return(output_np,y_np)
        
        
        
        
        
        
        
        
        
        
        
        
# ROOT Library
from ROOT import TMVA, TFile, TTree, TCut, TString
# DNN Library Pytorch
import torch
from torch.utils.data import DataLoader, TensorDataset
from torch import Tensor
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
# Optimizer SWATS
import swats
#numpy, pandas, matplotlib
import numpy as np
import cuda_guass_normal
from importlib import reload
import pandas as pd
import h5py

from array import array
from .model import *
from .train import *
from .preprocess import *
from .network_core import *

def apply_dnn(modellist, isRes, filename, treename, varname, branchname, mean_vec, var_vec):
    num_model = len(modellist)
    dim = len(varname)
    print(num_model,"models in total")
    file = TFile.Open(filename)
    dataTree = file.Get(treename)
    dataArray = dataTree.AsMatrix(varname)
    reload(cuda_guass_normal)
    dataSample = cuda_guass_normal.guass_normal_cuda(
        dataArray, mean_vec, var_vec)
    #Send DNN to GPU
    device = torch.device("cuda:0")
    i=0
    for modelname in modellist:
        print("Applying model ",modelname)
        if(isRes==0):
            model = Net(dim).to(device)
        if(isRes==1):
            model=nn.Sequential(nn.utils.weight_norm(nn.Linear(dim,100),name='weight'),
                              Mish(),ResNet(100),ResNet(100),ResNet(100),ResNet(100),
                            nn.utils.weight_norm(nn.Linear(100,100),name='weight'),Mish(),
                            nn.utils.weight_norm(nn.Linear(100, 1),name='weight'),
                            nn.Sigmoid()).to(device)
        #Load saved DNN model
        print("Loading model ", modelname)
        model.load_state_dict(torch.load(modelname))
        #Prepare dataloader feed to DNN
        dataTensor = torch.tensor(dataSample, dtype=torch.float).to(device)
        dataLoader = DataLoader(dataTensor, batch_size=10000, shuffle=False)
        print("Applying.........")
        if(i==0): 
            dataScore = eval_pytorch(model, device, dataLoader)
        else:
            dataScore = dataScore+eval_pytorch(model, device, dataLoader)
        del dataTensor
        del dataLoader 
        torch.cuda.empty_cache()
        print("Finish model ", modelname)
        i=i+1
    file.Close()
    dataScore = dataScore/num_model
    print("Write DNN score to minitree: ")
    writefile = TFile.Open(filename, "update")
    writeTree = writefile.Get(treename)
    #Create new DNN branch
    DNN = array("f", [0])
    #Branch type: float
    writename = branchname+"/F"
    #Load new branch
    DNNBranch = writeTree.Branch(branchname, DNN, writename)
    datascore = dataScore[0:, 0]
    writescore = datascore
    i = 0
    #Write DNN score to minitree
    for Score in writescore:
        i = i+1
        if(i % 5000 == 0):
            print("looping: ", i)
        DNN[0] = Score
        DNNBranch.Fill()
    writeTree.Write()
    writefile.Write()
    writefile.Close()

    return(dataScore)


def apply_dnn_fill_miss(modellist, isRes, filename, treename, varname, weight_name, branchname, mean_vec1, var_vec, mean_vec,missing_value):
    num_model = len(modellist)
    dim = len(varname)-1
    print(num_model,"models in total")
    file = TFile.Open(filename)
    dataTree = file.Get(treename)
    dataArray = dataTree.AsMatrix(varname)
    data_df=pd.DataFrame(data=dataArray,columns=varname)
    data_df_cut = fill_missing_value_mean(data_df, weight_name, missing_value, mean_vec)
    data_df_cut = data_df_cut.drop(columns=[weight_name])
    dataArray1 = data_df_cut.values
    reload(cuda_guass_normal)
    dataSample = cuda_guass_normal.guass_normal_cuda(
        dataArray1, mean_vec1, var_vec)
    #Send DNN to GPU
    device = torch.device("cuda:0")
    i=0
    for modelname in modellist:
        print("Applying model ",modelname)
        if(isRes==0):
            model = Net(dim).to(device)
        if(isRes==1):
            model=nn.Sequential(nn.utils.weight_norm(nn.Linear(dim,100),name='weight'),
                              Mish(),ResNet(100),ResNet(100),ResNet(100),ResNet(100),
                            nn.utils.weight_norm(nn.Linear(100,100),name='weight'),Mish(),
                            nn.utils.weight_norm(nn.Linear(100, 1),name='weight'),
                            nn.Sigmoid()).to(device)
        #Load saved DNN model
        print("Loading model ", modelname)
        model.load_state_dict(torch.load(modelname))
        #Prepare dataloader feed to DNN
        dataTensor = torch.tensor(dataSample, dtype=torch.float).to(device)
        dataLoader = DataLoader(dataTensor, batch_size=10000, shuffle=False)
        print("Applying.........")
        if(i==0): 
            dataScore = eval_pytorch(model, device, dataLoader)
        else:
            dataScore = dataScore+eval_pytorch(model, device, dataLoader)
        del dataTensor
        del dataLoader 
        torch.cuda.empty_cache()
        print("Finish model ", modelname)
        i=i+1
    file.Close()
    dataScore = dataScore/num_model
    print("Write DNN score to minitree: ")
    writefile = TFile.Open(filename, "update")
    writeTree = writefile.Get(treename)
    #Create new DNN branch
    DNN = array("f", [0])
    #Branch type: float
    writename = branchname+"/F"
    #Load new branch
    DNNBranch = writeTree.Branch(branchname, DNN, writename)
    datascore = dataScore[0:, 0]
    writescore = datascore
    i = 0
    #Write DNN score to minitree
    for Score in writescore:
        i = i+1
        if(i % 5000 == 0):
            print("looping: ", i)
        DNN[0] = Score
        DNNBranch.Fill()
    writeTree.Write()
    writefile.Write()
    writefile.Close()

    return(dataScore)


def apply_pdnn(modellist, isRes, filename, treename, varname, branchname, mean_vec, var_vec,reFill, parameter_name, parameter_value):
    num_model = len(modellist)
    dim = len(varname)
    print(num_model,"models in total")
    file = TFile.Open(filename)
    dataTree = file.Get(treename)
    dataArray = dataTree.AsMatrix(varname)
    if(reFill==1):
        parameter_pos = varname.index(parameter_name)
        print("Refill variable",parameter_name)
        dataArray[:,parameter_pos]=parameter_value
        
    reload(cuda_guass_normal)
    dataSample = cuda_guass_normal.guass_normal_cuda(
        dataArray, mean_vec, var_vec)
    #Send DNN to GPU
    device = torch.device("cuda:0")
    i=0
    for modelname in modellist:
        print("Applying model ",modelname)
        if(isRes==0):
            model = Net(dim).to(device)
        if(isRes==1):
            model=nn.Sequential(nn.utils.weight_norm(nn.Linear(dim,100),name='weight'),
                              Mish(),ResNet(100),ResNet(100),ResNet(100),ResNet(100),
                            nn.utils.weight_norm(nn.Linear(100,100),name='weight'),Mish(),
                            nn.utils.weight_norm(nn.Linear(100, 1),name='weight'),
                            nn.Sigmoid()).to(device)
        #Load saved DNN model
        print("Loading model ", modelname)
        model.load_state_dict(torch.load(modelname))
        #Prepare dataloader feed to DNN
        dataTensor = torch.tensor(dataSample, dtype=torch.float).to(device)
        dataLoader = DataLoader(dataTensor, batch_size=10000, shuffle=False)
        print("Applying.........")
        if(i==0): 
            dataScore = eval_pytorch(model, device, dataLoader)
        else:
            dataScore = dataScore+eval_pytorch(model, device, dataLoader)
        del dataTensor
        del dataLoader 
        torch.cuda.empty_cache()
        print("Finish model ", modelname)
        i=i+1
    file.Close()
    dataScore = dataScore/num_model
    print("Write DNN score to minitree: ")
    writefile = TFile.Open(filename, "update")
    writeTree = writefile.Get(treename)
    #Create new DNN branch
    DNN = array("f", [0])
    #Branch type: float
    writename = branchname+"/F"
    #Load new branch
    DNNBranch = writeTree.Branch(branchname, DNN, writename)
    datascore = dataScore[0:, 0]
    writescore = datascore
    i = 0
    #Write DNN score to minitree
    for Score in writescore:
        i = i+1
        if(i % 5000 == 0):
            print("looping: ", i)
        DNN[0] = Score
        DNNBranch.Fill()
    writeTree.Write()
    writefile.Write()
    writefile.Close()

    return(dataScore)



def apply_dnn_new(modellist,  filename, treename, branchname, data_name, config_name, reFill, parameter_value):
    num_model = len(modellist)
    data_file = h5py.File(data_name,'r')
    data_file.keys()
    mean_vec = data_file["mean_vec"][:]
    var_vec = data_file["var_vec"][:]
    config_file = open(config_name,'r')
    temp_list = config_file.read()
    training_parameters = eval(temp_list)
    varname = training_parameters["training_variable"]
    ispDNN = training_parameters["ispDNN"]
    if(ispDNN):
        tag_variable = training_parameters["tag_variable"]
    dim = len(varname)
    print(num_model,"models in total")
    file = TFile.Open(filename)
    dataTree = file.Get(treename)
    dataArray = dataTree.AsMatrix(varname)
    if((reFill == 1)&(ispDNN)):
        parameter_pos = varname.index(tag_variable)
        print("Refill variable",tag_variable)
        dataArray[:,parameter_pos]=parameter_value
        
    reload(cuda_guass_normal)
    dataSample = cuda_guass_normal.guass_normal_cuda(
        dataArray, mean_vec, var_vec)
    i = 0
    for modelname in modellist:
        print("Applying model ",modelname)
        my_net = core(training_parameters)
        #Load saved DNN model
        print("Loading model ", modelname)
        my_net.load_model(modelname)
        print("Applying.........")
        if(i==0): 
            dataScore = my_net.predict(dataSample, 10000)
        else:
            dataScore = dataScore + my_net.predict(dataSample, 10000)
        torch.cuda.empty_cache()
        print("Finish model ", modelname)
        i=i+1
    file.Close()
    dataScore = dataScore/num_model
    print("Write DNN score to minitree: ")
    writefile = TFile.Open(filename, "update")
    writeTree = writefile.Get(treename)
    #Create new DNN branch
    if(training_parameters["num_class"] == 2):
        DNN = array("f", [0])
        #Branch type: float
        writename = branchname+"/F"
        #Load new branch
        DNNBranch = writeTree.Branch(branchname, DNN, writename)
        datascore = dataScore[0:, 0]
        writescore = datascore
        i = 0
        #Write DNN score to minitree
        for Score in writescore:
            i = i+1
            if(i % 5000 == 0):
                print("looping: ", i)
            DNN[0] = Score
            DNNBranch.Fill()
    else:
        for l in range(training_parameters["num_class"]):
            print("writing score for classifier"+str(l))
            branchname_temp = branchname+"_classifier"+str(l)
            writename_temp = branchname_temp+"/F"
            DNN = array("f", [0])
            DNNBranch = writeTree.Branch(branchname_temp, DNN, writename_temp)
            datascore = dataScore[0:, l]
            writescore = datascore
            i = 0 
            for Score in writescore:
                i = i+1
                if(i % 5000 == 0):
                    print("looping: ", i)
                DNN[0] = Score
                DNNBranch.Fill()
            
            
    writeTree.Write()
    writefile.Write()
    writefile.Close()

    return(dataScore)










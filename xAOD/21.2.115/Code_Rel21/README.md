xAOD to Minitree Framework
==================================

1.Basic info

------------------------------------------
shuzhouz@umich.edu
NOTE: Code under development. Do not change release or sample tags, etc.
Otherwise could have quite some technical WARNING/ERROR.

This framework is based on QuickAna. Please refer : `https://gitlab.cern.ch/ziwang/quickana`


Rel. 21.2.115, CMake based.

Support p-tag > 40xx

If you want to add new branch to minitree, please modify `source/MyAnalysis/share/MiniTree.txt`

pileup files are under `source/MyAnalysis/share`

General info, refer to
https://twiki.cern.ch/twiki/bin/view/AtlasComputing/RootCoreToCMake


2.Structure:

---------------------------------------------
 - `./source: analysis code`
 - `./build: compilation`
 - `./run: run folder`


3.First time setup:

----------------------------------------------

 - `rm -rf build` 
 - `mkdir build`
 - `source setup.sh`

Compilation
 - `cd build
 - `make`


4.How to run

------------------------------------------------
 - `cd run`
 - `./test_run.sh input.list`
 -  Please refer to `input_345071_16d.list` to see the input format
 -  Check your results under `/output`
 -  Histograms under `data-hist_output`
 -  Trees under `data-tree_output`

GoodRun list and luminosity please refer to: `https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/GoodRunListsForAnalysisRun2`



How to get new pileup files: `https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/ExtendedPileupReweighting`









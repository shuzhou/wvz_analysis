#include<iostream>
#include<fstream>
#include<stdlib.h>
#include<math.h>
#include<stdio.h>
#include<map>
#include<string>
#include <thread>
#include<algorithm>
#include <sstream>
#include <vector>

using namespace std;
static const int num_threads = 40;

void run(string input_name, string output_name, int num){
    char command[500];
    //cout<<"Starting job list: "<<input_name<<endl;
    sprintf(command,"runFourLep -outdir %s/output -in %s > %s/log.txt", output_name.c_str(), input_name.c_str(), output_name.c_str());
    //cout<<command<<endl;
    system(command);
    
}

int main(){
    ifstream in;
    string source_dir = "/atlas/data18a/shuzhouz/WVZ/21.2.115/Code_Rel21/";
    string work_dir = "/atlas/data18a/shuzhouz/WVZ/21.2.115/Code_Rel21/run/";
    string out_dir = "/atlas/data18a/shuzhouz/WVZ/21.2.115/Code_Rel21/run/";
    string list_name = "input_list.txt";
    string list_file;
    string file_name;
    int counter = 0;
    int n_threads;
    thread t[num_threads];
    int file_count = 0;
    list_file = work_dir;
    list_file+=list_name;
    char command1[500];
    sprintf(command1,"rm -rf %slist",work_dir.c_str());
    system(command1);
    char command2[500];
    sprintf(command2,"mkdir %slist",work_dir.c_str());
    system(command2);
    vector<string> dir_list;
    vector<string> list_file_list;
    in.open(list_file.c_str(),ios::out);
    while(in>>file_name){
        string temp_name = "input.splitted_list"+to_string(file_count);
        char temp_command[500];
        sprintf(temp_command,"mkdir %s%s",out_dir.c_str(),temp_name.c_str());
        dir_list.push_back(out_dir+temp_name);
        system(temp_command);
        ofstream out;
        string file_temp = work_dir+"list/"+temp_name;
        list_file_list.push_back(file_temp);
        out.open(file_temp.c_str());
        out<<file_name;
        out.close();
        file_count++;
        
    }
    
    
    for(int i =file_count; i>0; i=i-num_threads){
        if((i)<num_threads){
            n_threads = i;
        }
        else{
            n_threads = num_threads;
        }
        for(int j=0;j<n_threads;j++){
            cout<<"starting file "<<counter+j<<endl;
            t[j] = thread(run,list_file_list[counter+j],dir_list[counter+j],counter+j);
           // cout<<"file "<<counter+j<<" complete"<<endl;
            
        }
        for(int j=0;j<n_threads;j++){
            t[j].join();
            cout<<"file "<<counter+j<<" complete"<<endl;
        }
        counter = counter + n_threads;
        
    }
    return(0);
    
}
